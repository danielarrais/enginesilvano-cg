package com.sharknado.aplicacaografica;

import com.sharknado.aplicacaografica.AndGraph.AGGameManager;
import com.sharknado.aplicacaografica.AndGraph.AGInputManager;
import com.sharknado.aplicacaografica.AndGraph.AGScene;
import com.sharknado.aplicacaografica.AndGraph.AGScreenManager;
import com.sharknado.aplicacaografica.AndGraph.AGSprite;

public class CenaConf extends AGScene {

    AGSprite gato;

    int animacao = 0;
    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public CenaConf(AGGameManager pManager) {
        super(pManager);
    }

    @Override
    public void init() {
        //Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(1,0,1);

        gato = createSprite(R.mipmap.gato, 8, 8);
        gato.setScreenPercent(10, 20);
        gato.vrPosition.setXY(AGScreenManager.iScreenWidth/2, AGScreenManager.iScreenHeight/2);
        gato.addAnimation(15, true,0,15);
        gato.addAnimation(10, true,16,28);
        gato.addAnimation(15, true,19,40);
        gato.iMirror = AGSprite.HORIZONTAL;
    }

    @Override
    public void restart() {
        //Chamado após o retorno de uma interrupção
    }

    @Override
    public void stop() {
        //Chamado quando uma interrupção acontecer
    }

    @Override
    public void loop() {
        //Chamado n vezes por segundo

        if (AGInputManager.vrTouchEvents.screenClicked()){
            animacao++;
            if (animacao == 3)
                animacao = 0;

            gato.setCurrentAnimation(animacao);
        }
    }
}