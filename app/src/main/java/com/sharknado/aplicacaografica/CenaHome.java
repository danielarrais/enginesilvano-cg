package com.sharknado.aplicacaografica;

import com.sharknado.aplicacaografica.AndGraph.AGGameManager;
import com.sharknado.aplicacaografica.AndGraph.AGInputManager;
import com.sharknado.aplicacaografica.AndGraph.AGScene;
import com.sharknado.aplicacaografica.AndGraph.AGScreenManager;
import com.sharknado.aplicacaografica.AndGraph.AGSprite;
import com.sharknado.aplicacaografica.AndGraph.AGTimer;

//Classe que irá representar uma cena ou contexto da aplicação
public class CenaHome extends AGScene {

    AGTimer tempo = null;
    AGSprite play = null;
    AGSprite exit = null;
    AGSprite conf = null;
    AGSprite logo = null;

    /*******************************************
     * Name: CAGScene()
     * Description: Scene construtor
     * Parameters: CAGameManager
     * Returns: none
     *****************************************
     * @param pManager*/
    public CenaHome(AGGameManager pManager) {
        super(pManager);
    }

    @Override
    public void init() {
        //Chamada toda vez que a cena for ativada, exibida
        setSceneBackgroundColor(1,1,0);
        tempo = new AGTimer(3000);

        //Adciona uma imagem a cena
        play = createSprite(R.mipmap.play, 1,1);
        play.setScreenPercent(15,15);

        conf = createSprite(R.mipmap.conf, 1,1);
        conf.setScreenPercent(15,15);

        exit = createSprite(R.mipmap.exit, 1,1);
        exit.setScreenPercent(15,15);

        logo = createSprite(R.mipmap.logo, 1,1);
        logo.setScreenPercent(60,30);

        //Configura a Posição do play
        logo.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)+500);
        play.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)-100);
        conf.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)-400);
        exit.vrPosition.setXY(AGScreenManager.iScreenWidth/2, (AGScreenManager.iScreenHeight/2)-700);
    }

    @Override
    public void restart() {
        //Chamado após o retorno de uma interrupção
    }

    @Override
    public void stop() {
        //Chamado quando uma interrupção acontecer
    }

    @Override
    public void loop() {
        //Chamado n vezes por segundo

        //Atualiza o tempo
        tempo.update();

        //Verifica se o tempo já se esgotou
//        if (tempo.isTimeEnded()){
//            vrGameManager.setCurrentScene(1);
//        }

        //chama outra cena
//        if (AGInputManager.vrTouchEvents.screenClicked()){
//            vrGameManager.setCurrentScene(1);
//        }

        if(AGInputManager.vrTouchEvents.screenClicked()){
            if(play.collide(AGInputManager.vrTouchEvents.getLastPosition())){
                vrGameManager.setCurrentScene(1);
                return;
            }
            if(conf.collide(AGInputManager.vrTouchEvents.getLastPosition())){
                vrGameManager.setCurrentScene(2);
                return;
            }
            if(exit.collide(AGInputManager.vrTouchEvents.getLastPosition())){
                vrGameManager.setCurrentScene(3);
                return;
            }
        }
    }
}
